#!/usr/bin/env python3

# TODO: Use proper streams, not read the entire file first.
# TODO: Control the regular expression pair by an arguments.
# TODO: Rewrite in Guile, functionally and with continuations.

import os, re

bb = (r'(\(\*)', r'(\*\))')

def sm (s):
    """Stream of Matches"""
    p = 0
    while True:
        m = re.search (r'|'.join (bb), s[p:])
        if m is None: return
        yield (p, m)
        p += m.end ()

def sd (s):
    """Stream of Differences"""
    for p, m in sm (s):
        i, d = (m.end (), -1) if m.group (1) is None else (m.start (), 1)
        yield (p + i, d)

def sa (s):
    """Stream of Accumulated"""
    a = 0
    for i, d in sd (s):
        a += d
        yield (i, a)

def sg (s):
    """Stream of Groups"""
    i = None
    l = 0
    for j, a in sa (s):
        if i is None: i = j
        if a == l:
            yield (i, j)
            i = None

def sr (s):
    """Stream of Remains"""
    t = 0
    for b, e in sg (s):
        yield s[t:b]
        t = e
    yield s[t:]

os.sys.stdout.write (''.join (sr (os.sys.stdin.read ())))

# echo -e 'A(*B(*C\n*)D*)E\nfoo F(*G(*\nH*)I*)J' | ./ncstrip.py
